#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import pytest
from unittest import mock

from mymodule import rm


@mock.patch('mymodule.os.path')
@mock.patch('mymodule.os')
def test_rm(mock_os, mock_path):
    # set up the mock
    mock_path.isfile.return_value = False
    rm("/my/test/path")

    # test that the remove call was NOT called.
    assert not mock_os.remove.called, \
        "Failed to not remove the file if not present."

    # make the file 'exist'
    mock_path.isfile.return_value = True
    rm("/my/test/path")

    # test that rm called os.remove with the right parameters
    mock_os.remove.assert_called_with("/my/test/path")
